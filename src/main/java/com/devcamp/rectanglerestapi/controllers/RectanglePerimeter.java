package com.devcamp.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rectanglerestapi.models.Rectangle;

@RestController
@RequestMapping("/")
public class RectanglePerimeter {
    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam("width") float width, @RequestParam("length") float length){
        
        Rectangle rectangle = new Rectangle(length, width);
        double perimeter = rectangle.getPerimeter();
        return perimeter;
    }
}
