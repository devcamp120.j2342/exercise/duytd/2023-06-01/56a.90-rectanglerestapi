package com.devcamp.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.FlashMap;

import com.devcamp.rectanglerestapi.models.Rectangle;

@RestController
@RequestMapping("/")
public class RectangleArea {
    @GetMapping("/rectangle-area")
    public double getRectangleArea(@RequestParam("width") float width, @RequestParam("length") float length){
        
        Rectangle rectangle = new Rectangle(length, width);
        double area = rectangle.getArea();
        
        return area;
    }
}
